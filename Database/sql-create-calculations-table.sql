CREATE TABLE `calculations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(15) NOT NULL DEFAULT '',
  `timestamp` int(11) NOT NULL,
  `operation` varchar(250) NOT NULL DEFAULT '',
  `result` decimal(12,4) NOT NULL,
  `bonus` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;