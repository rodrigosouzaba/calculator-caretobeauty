<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Calculation $calculation
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $calculation->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $calculation->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Calculations'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="calculations form large-9 medium-8 columns content">
    <?= $this->Form->create($calculation) ?>
    <fieldset>
        <legend><?= __('Edit Calculation') ?></legend>
        <?php
            echo $this->Form->control('ip');
            echo $this->Form->control('timestamp');
            echo $this->Form->control('operation');
            echo $this->Form->control('result');
            echo $this->Form->control('bonus');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
