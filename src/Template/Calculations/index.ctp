<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Calculation[]|\Cake\Collection\CollectionInterface $calculations
 */
?>

    <h3><?= __('Calculations') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ip') ?></th>
                <th scope="col"><?= $this->Paginator->sort('timestamp') ?></th>
                <th scope="col"><?= $this->Paginator->sort('operation') ?></th>
                <th scope="col"><?= $this->Paginator->sort('result') ?></th>
                <th scope="col"><?= $this->Paginator->sort('bonus') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($calculations as $calculation): ?>
            <tr>
                <td><?= $this->Number->format($calculation->id) ?></td>
                <td><?= h($calculation->ip) ?></td>
                <td><?= date("d/m/Y",$calculation->timestamp) ?></td>
                <td><?= h($calculation->operation) ?></td>
                <td><?= $this->Number->format($calculation->result) ?></td>
                <td><?= ($calculation->bonus== 0 ? "false" : "true") ?></td>
                <td class="actions">
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $calculation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $calculation->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
