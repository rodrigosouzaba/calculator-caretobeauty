<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Calculation $calculation
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Calculation'), ['action' => 'edit', $calculation->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Calculation'), ['action' => 'delete', $calculation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $calculation->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Calculations'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Calculation'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="calculations view large-9 medium-8 columns content">
    <h3><?= h($calculation->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Ip') ?></th>
            <td><?= h($calculation->ip) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Operation') ?></th>
            <td><?= h($calculation->operation) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($calculation->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Timestamp') ?></th>
            <td><?= $this->Number->format($calculation->timestamp) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Result') ?></th>
            <td><?= $this->Number->format($calculation->result) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Bonus') ?></th>
            <td><?= $calculation->bonus ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
