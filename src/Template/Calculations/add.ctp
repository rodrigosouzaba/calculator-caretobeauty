

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Calculation $calculation
 */
?>
<div class="container">
    <div class="row text-center">
        <div class="col-sm-12 col-md-8 offset-md-2">

            <h3>New Calculation</h3>
            <h6 class="text-muted" role="alert">
                Your IP Address is: <b><?= $ip->ip ?></b>
            </h6>
            <div class="card">
                <div class="card-header">
                    <p class="h3 no-margin" id="screen">
                        0
                    </p>
                </div>
                <?= $this->Form->create();?>
                <?= $this->Form->hidden("operation", ["value" =>'', "id" => "operation"]);?>
                <?= $this->Form->end();?>
                <div class="card-body">
                    
                    
                    
                    <div class="container">
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-warning btn-block clear-screen" ><b>AC</b></button>
                            </div>
                            <div class="col-3">
                                <button type="button" class="btn btn-warning btn-block value" value="%"  symbol="operator"><b>MOD</b></button>
                            </div>
                            <div class="col-3">
                                <button type="button" class="btn btn-warning btn-block value" value="/" symbol="operator"><i class="fas fa-divide"></i></button>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col">
                                <button type="button" class="btn btn-dark btn-block value" value="7">7</button>
                            </div>
                            <div class="col">
                                <button type="button" class="btn btn-dark btn-block value" value="8">8</button>
                            </div>
                            <div class="col">
                                <button type="button" class="btn btn-dark btn-block value" value="9">9</button>
                            </div>
                            <div class="col">
                                <button type="button" class="btn btn-warning btn-block value" value="*" symbol="operator"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col">
                                <button type="button" class="btn btn-dark btn-block value" value="4">4</button>
                            </div>
                            <div class="col">
                                <button type="button" class="btn btn-dark btn-block value" value="5">5</button>
                            </div>
                            <div class="col">
                                <button type="button" class="btn btn-dark btn-block value" value="6">6</button>
                            </div>
                            <div class="col">
                                <button type="button" class="btn btn-warning btn-block value" value="-" symbol="operator"><i class="fas fa-minus"></i></button>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col">
                                <button type="button" class="btn btn-dark btn-block  value" value="1">1</button>
                            </div>
                            <div class="col">
                                <button type="button" class="btn btn-dark btn-block value" value="2">2</button>
                            </div>
                            <div class="col">
                                <button type="button" class="btn btn-dark btn-block value" value="3">3</button>
                            </div>
                            <div class="col">
                                <button type="button" class="btn btn-warning btn-block value" value="+" symbol="operator"><i class="fas fa-plus"></i></button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-dark btn-block  value" value="0">0</button>
                            </div>
                            <div class="col">
                                <button type="button" class="btn btn-dark btn-block value" value="." symbol="operator">.</button>
                            </div>
                            <div class="col">
                                <button type="button" class="btn btn-warning btn-block result" value="=" symbol="equal"><i class="fas fa-equals"></i></button>
                            </div>
                            <div id="resultado"></div> 
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
    <div id="bonus-status" class=""></div>
</div>
<script type="text/javascript"> 
    $(document).ready(function() {
        $("#operation").val("");
    });
    $(".value").click(function(e){
        e.preventDefault();
        //Check if user clicked at a Operation Symbol (/,-,+,x,MOD)
        if($(this).attr("symbol") === "operator"){
            var lastChar = $("#screen").text().substr($("#screen").text().length - 1);

            //If lastChar is a Operation Symbol, change the operation 
            if(isNaN(lastChar) == true)
            {
                //Substitute the operator symbol by the "new" operator cymbol clicked
                var operation = $("#screen").text().substr(0, $("#screen").text().length-1);
                $("#screen").html(operation);
                $("#operation").val(operation);

            }
        }

        if($("#screen").text() == 0){
            $("#screen").html("");
        }
        if($("#screen").text() == "Error: Division by 0")
        {
            $("#screen").html("");
            $("#operation").val("");
        }
        var a = $(this).attr("value");
        $("#screen").append(a);
        $("#operation").val($("#operation").val() + a);
    });

     $(".result").click(function(){
        $.ajax({
            type: "POST",
            url: "<?php echo $this->request->webroot ?>calculations/add/",
            data: $("#operation").serialize(),
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
            },
            complete: function(data) {
                // console.log(data);
                $("#screen").empty();
                $("#screen").append(data["responseJSON"]["result"]);
                $("#operation").val(data["responseJSON"]["result"]);
                if(data["responseJSON"]["bonus"] === true){
                    $("#bonus-status").addClass("text-center alert alert-success");
                    $("#bonus-status").append("Congrats! You just won a <b>Care to Beauty</b> Prize! Your calculation match a winning number!");
                }else{
                    $("#bonus-status").removeClass("text-center alert alert-success");
                    $("#bonus-status").empty();
                }
           }
        });
     });

     $(".clear-screen").click(function(){
          $("#operation").val("");
          $("#screen").html("0");
     });


</script>