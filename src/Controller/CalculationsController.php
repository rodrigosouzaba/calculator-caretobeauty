<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Calculations Controller
 *
 * @property \App\Model\Table\CalculationsTable $Calculations
 *
 * @method \App\Model\Entity\Calculation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CalculationsController extends AppController
{
    
    public $result;
    public $bonus;
    public $randomNumberBonus;
    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $calculations = $this->paginate($this->Calculations);

        $this->set(compact('calculations'));
    }

    /**
     * View method
     *
     * @param string|null $id Calculation id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $calculation = $this->Calculations->get($id, [
            'contain' => []
        ]);

        $this->set('calculation', $calculation);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($operation = null, $result = null)
    {
        //Create an object to check ip address from client browser
        $ip = new IpsController();
        $ip->setIp();

        $json = null;

        $calculation = $this->Calculations->newEntity();
        if ($this->request->is('post')) {

            $this->calculate($this->request->data["operation"]);
            $this->setRandomNumberBonus();
            $this->checkMatchBonus();
            //Creates an array with data to save in the database
            $data = array();
            $data["ip"] = $ip->ip;
            $data["timestamp"] = time();
            $data["operation"] = $this->request->data["operation"];
            $data["result"] = $this->getResult();
            $data["bonus"] = $this->getBonus();
            
            $calculation = $this->Calculations->patchEntity($calculation, $data);
            // debug($this->Calculations->save($calculation));
            if ($this->Calculations->save($calculation)) {
                return $this->response->withType("application/json")->withStringBody(json_encode($data));
            }else{
                return $this->response->withType("application/json")->withStringBody(json_encode($data));
                // return $this->response->withType("application/json")->withStringBody($this->getResult());
            }
        }
        $this->set(compact('ip'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Calculation id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $calculation = $this->Calculations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $calculation = $this->Calculations->patchEntity($calculation, $this->request->getData());
            if ($this->Calculations->save($calculation)) {
                $this->Flash->success(__('The calculation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The calculation could not be saved. Please, try again.'));
        }
        $this->set(compact('calculation'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Calculation id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $calculation = $this->Calculations->get($id);
        if ($this->Calculations->delete($calculation)) {
            $this->Flash->success(__('The calculation has been deleted.'));
        } else {
            $this->Flash->error(__('The calculation could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    /**
     * Calculate method
     *
     * @param string|null $id Calculation id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function calculate($expression = null)
    {
        //Separating all elements in an array
        $components = preg_split('~(?<=\d)([*%/+-])~',$expression,NULL,PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

        //Create 2 separeted arrays with numbers and operations
        foreach($components as $element)
        {
            if(is_numeric($element))
            {
                $numbers[] = $element;
            }else{
                $operators[] = $element;
            }
        }
        
        $result = null;
        $redFlag = null;

        //Realize the calculations 
        foreach($numbers as $key => $value )
        {
            if(empty($result)){
                $result = $value;
            }else{
                if($redFlag <> true){

                    if (isset($operators[$key-1])) {
                        switch($operators[$key-1])
                        {
                            case "+":
                                $result = $result + $value; 
                            break;
                            case "-":
                                $result = $result - $value; 
                            break;
                            case "*":
                                $result = $result * $value; 
                            break;
                            case "/":
                                //the is a division operation and the second operator is 0
                                if($value == 0)
                                {
                                    //Division by 0 shows an error message
                                    $result = "Error: Division by 0";
                                    $redFlag = true;
                                }else{
                                    $result = $result / $value; 
                                }
                                break;
                                case "%":
                                $result = $result % $value; 
                                break;
                            }
                        }
                    }else{
                        //end the foreach loop beacause the division by 0
                        break;
                    }
            }
        }
        $this->setResult($result);
    }

    /**
     * Result Settter method
     *
     * @param integer $result value calculated.
     * @return \Cake\Http\Response|void
     */
    public function setResult($result)
    {
        $this->result = $result;
    }
   
    /**
     * Result Gettter method
     *
     * @return \Cake\Http\Response|void
     */
    public function getResult()
    {
        return $this->result;
    }
    
    /**
     * Ramdom Number Settter method
     *
     * @return \Cake\Http\Response|void
     */
    public function setRandomNumberBonus()
    {
        $this->randomNumberBonus = rand(0,999999);

        // To test  the match uncomment  this line and  calculate 4 number
        // $this->randomNumberBonus = 4;
    }

    /**
     * Ramdom Number Gettter method
     *
     * @return \Cake\Http\Response|void
     */
    public function getRandomNumberBonus()
    {
        return $this->randomNumberBonus;
    }

    /**
     * Bonus Settter method
     *
     */
    public function setBonus($bonus)
    {
        $this->bonus = $bonus;
    }

    /**
     * Bonus Gettter method
     *
     * @return \Cake\Http\Response|void
     */
    public function getBonus()
    {
        return $this->bonus;
    }

    /**
     * Compare $this->result with $this->randomNumberBonus method
     *
     */
    public function checkMatchBonus()
    {
        $result = intval($this->getResult());
        $randomNumberBonus = intval($this->getRandomNumberBonus());

        if($result === $randomNumberBonus){
            $matchBonus = true;
        }else{
            $matchBonus = false;
        }

        $this->setBonus($matchBonus);
    }


    /**
     * Set the return value as a JSON
     *
     * @param string|null $result that can be an error message or a number.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function resultJson()
    {
        //Converting Result to JSON
        // $resultJ = json_encode(array('result' => $this->getResult()));
        // $this->response->type('json');
        // $this->response->body($resultJ);
        // return $this->response;
        return $this->response->withType("application/json")->withStringBody($this->getResult());
    }
}
