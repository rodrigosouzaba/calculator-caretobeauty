<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Ips Controller
 *
 *
 * @method \App\Model\Entity\Calculation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class IpsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */

    public $ip;

    public function getIp()
    {
        return $this->ip;
    }

    public function setIp()
    {
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            //local ip address
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $this->ip = $ip;
    }
}
