<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Calculations Model
 *
 * @method \App\Model\Entity\Calculation get($primaryKey, $options = [])
 * @method \App\Model\Entity\Calculation newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Calculation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Calculation|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Calculation saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Calculation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Calculation[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Calculation findOrCreate($search, callable $callback = null, $options = [])
 */
class CalculationsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('calculations');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('ip')
            ->maxLength('ip', 15)
            ->allowEmptyString('ip', false);

        $validator
            ->integer('timestamp')
            ->requirePresence('timestamp', 'create')
            ->allowEmptyString('timestamp', false);

        $validator
            ->scalar('operation')
            ->maxLength('operation', 250)
            ->allowEmptyString('operation', false);

        $validator
            ->decimal('result')
            ->requirePresence('result', 'create')
            ->allowEmptyString('result', false);

        $validator
            ->boolean('bonus')
            ->requirePresence('bonus', 'create')
            ->allowEmptyString('bonus', false);

        return $validator;
    }
}
